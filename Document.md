# Javajan
**Innovació dins l'empresa**

Aquest document es el primer de molts que defineixen totes les teconologies d'innovació que s'estan incorporant a l'empresa **Serveis d'Internet Javajan SL**.

A continuació disposeu d'una llista de tecnologíes innovadores que facilitarien la feina relacionada amb l'ambit principal de l'empresa:





### IDE de desnvolupament
Aquesta secció presenta el diferent software que utilitzem per desenvolupar els nostres productes.
- SublimeText 3 Beta (unregistred)
- NetBeans IDE
- Microsoft Code



### Desenvolupament d'aplicacions Mòvils
Aquesta secció explicarà els diferents projectes que s'estan realitzant dins l'empresa i que ha permés abraçar altres quotes de mercats desconeguts fins ara.
- iOS
- Android
- Ionic Framework (iOS + Android)
- Altres:
    - Socket.IO: Comunicació en temps real entre plataformes



### Project Manager. Organització de la feina
En aquest apartat definim les diferents metodologies i aplicacions que fem servir per gestionar els temps dels projectes, tenir un control de l'estat de cada projecte i participació activa de tot l'equip.
En resum definirem els següents programes:
- Trello.
- GitLab Issues.

#### Trello

> Explicació de trello

#### GitLab Issues

> Explicació del funcionament de gitlab issues and milestones.





### Control de versions
#### Git

[Pàgina oficial] (http://git-scm.com/)

> Git és un programari de sistema de control de versions dissenyat per Linus Torvalds, pensat en l'eficiència i confiabilitat de manteniment de versions d'aplicacions amb una enorme quantitat de fitxers de codi font.
>
> El disseny de Git es va basar en BitKeeper i en Monotone. En un principi, Git es va pensar com un motor de baix nivell que altres puguin emprar per escriure front end com Cogito o StGIT. Emperò, Git s'ha convertit des d'ençà en un sistema de control de versions amb funcionalitat plena. N'hi ha de molta rellevància que ja empren Git, en particular el grup de programació del nucli del sistema operatiu Linux.
[Wikipedia] (https://es.wikipedia.org/wiki/Markdown)

El problema al desenvolupar software radica en que hi participen més d'una persona. Per exemple, en projectes web hi ha diferents rols que construeixen conjuntament, i a vegades al mateix temps, la pàgina web: 

- Comercial
- Analista
- Dissenyador/a
- Maquetador/a
- Programador/a

Per poder mantenir organitzat el projecte i no tenir problemes alhora de que diferents integrants del projecte editin alhora, i amb diferents editors, el projecte es necessari incorporar eines que ens assegurin la integritat del projecte.
Git ens permet:

- Tenir diferents programadors programant diferents parts del projecte amb seguretat.
- Copies de seguretat per cada modificació de fitxer.
- Integració amb eines d'analisi de codi i visualització de l'estat del projecte.
- Compartir de forma segura el codi del projecte.

#### GitLab.com

[Pàgina oficial] (https://gitlab.com/)

PAAS que ens permet disposar d'un ric panell de control de tots els nostres projectes. 

Podem crear projectes privats, grups de programadors, permisos, controlar incidencies, etc. Tot de manera gratuita. Podem revisar al navegador cadascun dels fitxers del projecte i comparar-los amb fitxers de versions anteriors per detectar
incidencies i resoldre errors. **Tot això es possible amb la tecnologia GIT**.

Si ja saps que es GitHub, doncs podriem dir que GitLab es el seu clon opensource.





### Metodología de programació
Hem fet un pas endevant i estem adoptant les metodologies de feina actuals. Aquestes ens permeten mantenir un codi net, mantenible i escalable.
- MVC (Modelo Vista Controlador)
- OOP (Programación Orientada a Objetos)
- Utilització de Frameworks (Krang Framework, Laravel)







### Documentació de projectes
#### Markdown

> Markdown és un llenguatge de marques lleuger, originalment creat per John Gruber i Aaron Swartz que permet "escriure utilitzant un format de text planer fàcil d'escriure i de llegir i després convertir-ho en un XHTML o HTML estructuralment vàlid". El llenguatge té moltes similituds amb les convencions que ja existeixen a l'hora de formatar text planer en correus electrònics.
> 
> Markdown també és un script de Perl escrit per Gruber, Markdown.pl, que converteix text formatat a XHTML o HTML vàlid i també substitueix parèntesis angular cap a l'esquerra ('<') i les I comercials per les seves corresponents referències d'entitat de caràcter. Es pot fer servir com un script tot sol o bé com in plugin de Blosxom o de Movable Type i també com un filtre de text per BBEdit.
> 
> Markdown s'ha re-implementat per altra gent com un mòdul de Perl disponible a CPAN (Text::Markdown) i a una gran varietat de llenguatges de programació. És distribuït sota un llicència de tipus BSD i s'inclou en, o és disponible com un plugin per forces sistemes de gestió de continguts.
[Wikipedia] (https://es.wikipedia.org/wiki/Markdown)

Per exemple, aquest document es escrit amb Markdown i renderitzat de forma dinamica, també existeixen eines gratuites per passar aquest pre-rended text a PDF. Això facilita molt la distribució de la documentació del software programat i realitzat per part de l'empresa.